/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "Actors/GenericTwinStaticMeshActor.h"
#include "Components/GenericTwinStaticMeshComponent.h"

#include "Common/Scene/Scene.h"
#include "Common/Material/GenericTwinMaterialManager.h"
#include "Common/Material/MaterialProxy.h"

#include "ProceduralMeshConversion.h"
#include "MeshDescription.h"

// Sets default values
AGenericTwinStaticMeshActor::AGenericTwinStaticMeshActor()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	if(Root)
	{
		SetRootComponent(Root);
		Root->SetMobility(EComponentMobility::Movable);

		MeshComponent = GenericTwin::IStaticMeshComponent::Create(this);
		if(MeshComponent)
		{
			USceneComponent *sceneCmp = MeshComponent->GetSceneComponent();
			if(sceneCmp)
			{
				sceneCmp->AttachToComponent(Root, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
				sceneCmp->SetMobility(EComponentMobility::Movable);
			}
		}
	}
}

void AGenericTwinStaticMeshActor::Initialize()
{
	if(MeshComponent)
	{
		MeshComponent->Initialize(GenericTwin::IStaticMeshComponent::EUploadMode::Background);
	}
}

void AGenericTwinStaticMeshActor::Release()
{
	ClearMesh();

	m_AdditionalTextures.Empty();
}

void AGenericTwinStaticMeshActor::ClearMesh()
{
	if (MeshComponent)
	{
		MeshComponent->Clear();

		m_nextMeshSectionIndex = 0;
		m_EstimatedGPUMemoryUsage = 0;

		m_Materials.Empty();
	}
}

void AGenericTwinStaticMeshActor::AddMeshSection(const GenericTwin::SMeshSection &MeshSection)
{
	AddMeshSection(MeshSection, MeshSection.Material);
}

void AGenericTwinStaticMeshActor::AddMeshSection(const GenericTwin::SMeshSection &MeshSection, UMaterialInterface *Material)
{
	if (MeshComponent)
	{
		MeshComponent->AddMeshSection(MeshSection, Material);

		m_EstimatedGPUMemoryUsage += 12 * static_cast<uint32> (MeshSection.Vertices.Num());
		m_EstimatedGPUMemoryUsage += 12 * static_cast<uint32> (MeshSection.Normals.Num());
		m_EstimatedGPUMemoryUsage += 8 * static_cast<uint32> (MeshSection.UV0.Num());
		m_EstimatedGPUMemoryUsage += 4 * static_cast<uint32> (MeshSection.Indices.Num());

	}
}

void AGenericTwinStaticMeshActor::SetFlattenedScene(const GenericTwin::ScenePtr &ScenePtr)
{
	if	(	MeshComponent
		&&	ScenePtr
		)
	{
		MeshComponent->Clear();
		m_EstimatedGPUMemoryUsage = ScenePtr->EstimateGeometryMemorySize();

		UGenericTwinMaterialManager &matMgr = UGenericTwinMaterialManager::GetInstance();
		for(m_nextMeshSectionIndex = 0; m_nextMeshSectionIndex < ScenePtr->GetMeshCount(); ++m_nextMeshSectionIndex)
		{
			const GenericTwin::SSceneMesh *curMesh = ScenePtr->GetMesh(m_nextMeshSectionIndex);

			const GenericTwin::SSceneMaterial *sceneMaterial = ScenePtr->GetSceneMaterial(curMesh->MaterialName);
			if	(	sceneMaterial
				&&	sceneMaterial->MaterialPtr
				&&	sceneMaterial->MaterialPtr->IsValid()
				)
			{
				m_Materials.Add(sceneMaterial->MaterialPtr);
				MeshComponent->AddMeshSection(curMesh->Indices, curMesh->Vertices, &curMesh->Normals, &curMesh->UV0, nullptr, nullptr, sceneMaterial->MaterialPtr->GetMaterial());
			}
		}
	}
}

void AGenericTwinStaticMeshActor::AddTextureProxy(const GenericTwin::TextureProxyPtr &TexProxyPtr)
{
	m_AdditionalTextures.Add(TexProxyPtr);
}

FMeshDescription AGenericTwinStaticMeshActor::GetMeshDescription()
{
	return /*MeshComponent ? BuildMeshDescription(CustomMeshComponent) :*/ FMeshDescription();
}

TSet<TObjectPtr<UMaterialInterface>> AGenericTwinStaticMeshActor::GetMaterials() const
{
	TSet<TObjectPtr<UMaterialInterface>> materials;
	// const int32 numSections = CustomMeshComponent->GetNumSections();
	// for (int32 i = 0; i < numSections; ++i)
	// 	materials.Add(MeshComponent->GetMaterial(i));

	return materials;
}
