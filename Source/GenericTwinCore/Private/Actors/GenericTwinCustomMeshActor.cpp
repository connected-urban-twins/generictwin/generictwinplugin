/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "Actors/GenericTwinCustomMeshActor.h"
#include "Components/GenericTwinCustomMeshComponent.h"

#include "Common/Scene/Scene.h"
#include "Common/Material/GenericTwinMaterialManager.h"
#include "Common/Material/MaterialProxy.h"

#include "ProceduralMeshConversion.h"
#include "MeshDescription.h"

// Sets default values
AGenericTwinCustomMeshActor::AGenericTwinCustomMeshActor()
{
	CustomMeshComponent = CreateDefaultSubobject<UGenericTwinCustomMeshComponent>(TEXT("CustomMeshComponent"));
	if(CustomMeshComponent)
	{
		SetRootComponent(CustomMeshComponent);
		CustomMeshComponent->SetMobility(EComponentMobility::Movable);
		CustomMeshComponent->bUseAsyncCooking = true;
	}

}

void AGenericTwinCustomMeshActor::Release()
{
	ClearMesh();

	m_AdditionalTextures.Empty();
}

void AGenericTwinCustomMeshActor::ClearMesh()
{
	if (CustomMeshComponent)
	{
		CustomMeshComponent->ClearAllMeshSections();

		m_nextMeshSectionIndex = 0;
		m_EstimatedGPUMemoryUsage = 0;

		m_Materials.Empty();
	}
}

void AGenericTwinCustomMeshActor::AddMeshSection(const GenericTwin::SMeshSection &MeshSection)
{
	AddMeshSection(MeshSection, MeshSection.Material);
}

void AGenericTwinCustomMeshActor::AddMeshSection(const GenericTwin::SMeshSection &MeshSection, UMaterialInterface *Material)
{
	if (CustomMeshComponent)
	{
		TArray< FLinearColor > colors;
		TArray< FProcMeshTangent > tangents;
		TArray< FVector2D > emptyUVs;

		CustomMeshComponent->CreateMeshSection_LinearColor(m_nextMeshSectionIndex, MeshSection.Vertices, MeshSection.Indices, MeshSection.Normals, MeshSection.UV0, MeshSection.UV1, emptyUVs, emptyUVs, colors, tangents, false);
		CustomMeshComponent->SetMaterial(m_nextMeshSectionIndex, Material);
		++m_nextMeshSectionIndex;

		m_EstimatedGPUMemoryUsage += 12 * static_cast<uint32> (MeshSection.Vertices.Num());
		m_EstimatedGPUMemoryUsage += 12 * static_cast<uint32> (MeshSection.Normals.Num());
		m_EstimatedGPUMemoryUsage += 8 * static_cast<uint32> (MeshSection.UV0.Num());
		m_EstimatedGPUMemoryUsage += 4 * static_cast<uint32> (MeshSection.Indices.Num());

	}
}

void AGenericTwinCustomMeshActor::SetFlattenedScene(const GenericTwin::ScenePtr &ScenePtr)
{
	if	(	CustomMeshComponent
		&&	ScenePtr
		)
	{
		CustomMeshComponent->ClearAllMeshSections();
		m_EstimatedGPUMemoryUsage = ScenePtr->EstimateGeometryMemorySize();

		TArray< FLinearColor > colors;
		TArray< FProcMeshTangent > tangents;

		UGenericTwinMaterialManager &matMgr = UGenericTwinMaterialManager::GetInstance();
		for(m_nextMeshSectionIndex = 0; m_nextMeshSectionIndex < ScenePtr->GetMeshCount(); ++m_nextMeshSectionIndex)
		{
			const GenericTwin::SSceneMesh *curMesh = ScenePtr->GetMesh(m_nextMeshSectionIndex);
			CustomMeshComponent->CreateMeshSection_LinearColor(m_nextMeshSectionIndex, curMesh->Vertices, curMesh->Indices, curMesh->Normals, curMesh->UV0, colors, tangents, true);

			const GenericTwin::SSceneMaterial *sceneMaterial = ScenePtr->GetSceneMaterial(curMesh->MaterialName);
#if 1
			if	(	sceneMaterial
				//&&	sceneMaterial->Material != nullptr
				)
			{
				CustomMeshComponent->SetMaterial(m_nextMeshSectionIndex, sceneMaterial->MaterialPtr->GetMaterial());
			}
#else
			if(sceneMaterial)
			{
				GenericTwin::MaterialProxyPtr materialPtr = matMgr.GetMaterial(*sceneMaterial);
				if	(	materialPtr
					&&	materialPtr->IsValid()
					)
				{
					m_Materials.Add(materialPtr);
					m_EstimatedGPUMemoryUsage += materialPtr->GetTextureMemorySize();
					CustomMeshComponent->SetMaterial(m_nextMeshSectionIndex, materialPtr->GetMaterial());
				}
			}
#endif
		}
	}
}

void AGenericTwinCustomMeshActor::AddTextureProxy(const GenericTwin::TextureProxyPtr &TexProxyPtr)
{
	m_AdditionalTextures.Add(TexProxyPtr);
}

FMeshDescription AGenericTwinCustomMeshActor::GetMeshDescription()
{
	return CustomMeshComponent ? BuildMeshDescription(CustomMeshComponent) : FMeshDescription();
}

TSet<TObjectPtr<UMaterialInterface>> AGenericTwinCustomMeshActor::GetMaterials() const
{
	TSet<TObjectPtr<UMaterialInterface>> materials;
	const int32 numSections = CustomMeshComponent->GetNumSections();
	for (int32 i = 0; i < numSections; ++i)
		materials.Add(CustomMeshComponent->GetMaterial(i));

	return materials;
}
