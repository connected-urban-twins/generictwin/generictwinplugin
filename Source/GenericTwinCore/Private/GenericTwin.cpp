/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "GenericTwin.h"

#include "Layers/LayerManager.h"

#include "Layers/Dependencies/LayerDependencies.h"

#include "Configuration/MainConfiguration.h"
#include "Configuration/LayersConfiguration.h"

#include "Common/Material/GenericTwinMaterialManager.h"
#include "Common/Material/GenericTwinTextureCache.h"
#include "Common/Caching/LocalFileCache.h"

#include "Common/JobQueue/GenericTwinJobDispatcher.h"

#include "Kismet/GameplayStatics.h"
#include "Dom/JsonObject.h"

#include "GeoReferencingSystem.h"

DEFINE_LOG_CATEGORY(LogGenericTwin);

AGenericTwin* AGenericTwin::GetGenericTwin(UObject* WorldContextObject)
{
	AGenericTwin *genericTwin = nullptr;

	if (UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(World, AGenericTwin::StaticClass(), Actors);
		int NbActors = Actors.Num();
		if (NbActors == 0)
		{
			UE_LOG(LogGenericTwin, Error, TEXT("GenericTwin actor not found."));
		}
		else if (NbActors > 1)
		{
			UE_LOG(LogGenericTwin, Error, TEXT("Multiple GenericTwin actors found. Only one actor should be used."));
		}
		else
		{
			genericTwin = Cast<AGenericTwin>(Actors[0]);
		}
	}

	return genericTwin;
}

AGenericTwin::AGenericTwin()
{
	PrimaryActorTick.bCanEverTick = true;

	EnvironmentControl = CreateDefaultSubobject<UGenericTwinEnvironmentControlCmp>(TEXT("EnvironmentControl"));
	AddOwnedComponent(EnvironmentControl);

}

AGenericTwin::~AGenericTwin()
{
}

void AGenericTwin::Initialize(FString ConfigurationFile)
{
	FString configFileName;

#if WITH_EDITORONLY_DATA

	configFileName = ConfigurationFile;
	
#else

	configFileName = FPaths::Combine(FPaths::LaunchDir(), "default_config.json");

	//	read from command line
	TArray<FString> tokens;
	TArray<FString> switches;
	FCommandLine::Parse(FCommandLine::Get(), tokens, switches);
	UE_LOG(LogGenericTwin, Log, TEXT("Command line tokens:"));
	for (auto &t : tokens)
	{
		UE_LOG(LogGenericTwin, Log, TEXT("%s"), *t);
		FString left;
		FString right;
		if(t.Split(TEXT("="), &left, &right, ESearchCase::IgnoreCase, ESearchDir::FromStart))
		{
			if(left.Compare(TEXT("config")) == 0)
			{
				configFileName = right;
				UE_LOG(LogGenericTwin, Log, TEXT("Config file name extracted from command line: %s"), *configFileName);
			}
		}
	}

#endif

	if(MainConfiguration::loadConfiguration(m_Configuration, configFileName.TrimQuotes()))
	{
		MainConfiguration::configureLocalFileCache(m_Configuration);

		m_WorldContext.World = GetWorld();
		m_WorldContext.GeoReferencingSystem = AGeoReferencingSystem::GetGeoReferencingSystem(GetWorld());
		m_WorldContext.Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

		MainConfiguration::configure(*(m_WorldContext.GeoReferencingSystem), m_Configuration);
		MainConfiguration::configure(*(m_WorldContext.Pawn), *(m_WorldContext.GeoReferencingSystem), m_Configuration);

		UGenericTwinMaterialManager::Create(GetWorld(), FName(TEXT("MaterialManager")));
		MainConfiguration::configureMaterialManager(m_Configuration);

		if(UGenericTwinTextureCache::Create(GetWorld(), FName(TEXT("TextureCache"))))
		{
			UGenericTwinTextureCache &texCache = UGenericTwinTextureCache::GetInstance();

			texCache.SetMaxTextureSize(8192);
			texCache.SetCompressionThreshold(128 * 128);
			UGenericTwinMaterialManager::GetInstance().SetTextureCache(&texCache);
		}

		if(EnvironmentControl)
		{
			EnvironmentControl->Initialize(m_Configuration.GetRootObject(), m_WorldContext.GeoReferencingSystem->OriginLongitude, m_WorldContext.GeoReferencingSystem->OriginLatitude);
		}

		m_WorldContext.LayerManager = NewObject<ULayerManager> (this, FName(TEXT("LayerManager")));
		if(m_WorldContext.LayerManager)
			m_WorldContext.LayerManager->Initialize(m_WorldContext, m_Configuration.getConfiguration<SLayersConfiguration>());
	}
}

void AGenericTwin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if	(	m_WorldContext.GeoReferencingSystem
		&&	m_WorldContext.Pawn
		)
	{

		ULocalPlayer *localPlayer = Cast<APlayerController> (m_WorldContext.Pawn->GetController())->GetLocalPlayer();
		if	(	localPlayer
			&&	localPlayer->ViewportClient
			&&	localPlayer->ViewportClient->Viewport
			)
		{
			FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
				localPlayer->ViewportClient->Viewport,
				GetWorld()->Scene,
				localPlayer->ViewportClient->EngineShowFlags).SetRealtimeUpdate(true));

			FVector viewLocation;
			FRotator viewRotation;
			m_WorldContext.SceneView = localPlayer->CalcSceneView(&ViewFamily, viewLocation, viewRotation, localPlayer->ViewportClient->Viewport);

			m_WorldContext.LayerManager->Update(DeltaTime);

			m_WorldContext.SceneView = 0;
		}
	}
}

void AGenericTwin::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(m_WorldContext.LayerManager)
	{
		m_WorldContext.LayerManager->Shutdown();
		m_WorldContext.LayerManager->ConditionalBeginDestroy();
		m_WorldContext.LayerManager = 0;
	}

	// material manager must be destroyed first, as materials might reference textures
	UGenericTwinMaterialManager::Destroy();
	UGenericTwinTextureCache::Destroy();

	FGenericTwinJobDispatcher::Shutdown();

	GenericTwin::LocalFileCache::Destroy();

	UGenericTwinClassFinder::Destroy();

}

FSceneView* AGenericTwin::getSceneView(APawn &ViewPawn)
{
	FSceneView* sceneView = 0;
	ULocalPlayer *localPlayer = Cast<APlayerController> (ViewPawn.GetController())->GetLocalPlayer();
	if	(	localPlayer
		&&	localPlayer->ViewportClient
		&&	localPlayer->ViewportClient->Viewport
		)
	{
		FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(
			localPlayer->ViewportClient->Viewport,
			GetWorld()->Scene,
			localPlayer->ViewportClient->EngineShowFlags).SetRealtimeUpdate(true));

		FVector viewLocation;
		FRotator viewRotation;
		sceneView = localPlayer->CalcSceneView(&ViewFamily, viewLocation, viewRotation, localPlayer->ViewportClient->Viewport);
	}
	return sceneView;
}

