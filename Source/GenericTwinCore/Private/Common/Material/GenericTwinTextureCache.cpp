// /**The MIT License (MIT)Copyright (c) 2023 - present, Hamburg Port AuthorityPermission is hereby granted, free of charge, to any person obtaining a copyof this software and associated documentation files (the “Software”), to dealin the Software without restriction, including without limitation the rightsto use, copy, modify, merge, publish, distribute, sublicense, and/or sellcopies of the Software, and to permit persons to whom the Software isfurnished to do so, subject to the following conditions:The above copyright notice and this permission notice shall be included inall copies or substantial portions of the Software.THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS ORIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS INTHE SOFTWARE.**/


#include "Common/Material/GenericTwinTextureCache.h"
#include "Common/Material/TextureProxy.h"

#include "Common/Caching/LocalFileCache.h"
#include "Common/Caching/CacheKey.h"

#include "Import/ImageImporter.h"
#include "Import/ImageImportConfiguration.h"
#include "Common/Image/TextureCompression.h"

#include "Engine/Texture2DArray.h"

#include <stddef.h>

DEFINE_LOG_CATEGORY(LogGenericTwinTextureCache);

uint32 UGenericTwinTextureCache::s_NextTextureId = 1;

void UGenericTwinTextureCache::Shutdown()
{
	for(auto &item : m_CachedTextures)
	{
		DestroyTexture(*(item.Value));
	}
	m_CachedTextures.Empty();

}

UGenericTwinTextureCache::UGenericTwinTextureCache()
{
}

UGenericTwinTextureCache::~UGenericTwinTextureCache()
{
}

bool UGenericTwinTextureCache::AddTexture(const FString &TextureUrl)
{
	if(m_TexturesByUrl.Contains(TextureUrl) == false)
	{
		GenericTwin::SImageImportConfiguration importCfg;
		importCfg.MaxImageSize = m_MaxTextureSize;
		GenericTwin::SImagePtr image = GenericTwin::ImageImporter::ImportImageFromFile(TextureUrl, importCfg);
		if(image)
		{
			AddTexture(TextureUrl, image, GenericTwin::FCacheKey(TextureUrl));
		}
	}

	return true;
}

bool UGenericTwinTextureCache::AddTexture(const FString &TextureUrl, const void *DataPtr, uint32 DataSize)
{
	if(m_TexturesByUrl.Contains(TextureUrl) == false)
	{
		GenericTwin::FCacheKey key = TextureUrl.IsEmpty() ? GenericTwin::FCacheKey(DataPtr, DataSize) : GenericTwin::FCacheKey(TextureUrl);
		GenericTwin::SCachedFilePtr cachedFile = GenericTwin::LocalFileCache::GetInstance().GetCachedFile(key);
		if	(	cachedFile
			&&	cachedFile->IsValid()
			)
		{
			const SCacheTexture *cachedTexture = static_cast<const SCacheTexture*> (cachedFile->Data);
			UTexture2D *texture = CreateTextureFromCache(cachedTexture->Width, cachedTexture->Height, cachedTexture->Format, cachedTexture->DataSize, cachedTexture->Data);
			if(texture)
			{
				const uint32 textureId = s_NextTextureId++;
				STexture *texData = new STexture(textureId, TextureUrl, texture, cachedTexture->DataSize);
				if(texData != 0)
				{
					m_CachedTextures.Add(textureId, texData);
					m_TexturesByUrl.Add(TextureUrl, texData);
				}
			}
		}
		else
		{
			GenericTwin::SImageImportConfiguration importCfg;
			importCfg.MaxImageSize = m_MaxTextureSize;
			GenericTwin::SImagePtr image = GenericTwin::ImageImporter::ImportImageFromMemory(TextureUrl, DataPtr, DataSize, importCfg);
			if(image)
			{
				AddTexture(TextureUrl, image, key);
			}
		}
	}
	return true;
}

void UGenericTwinTextureCache::AddTexture(const FString &TextureUrl, const GenericTwin::SImagePtr &Image, const GenericTwin::FCacheKey &Key)
{
	UTexture2D *texture = 0;
	uint32 texMemSize = 0;
	if	(	Image->GetFormat() == GenericTwin::SImage::EFormat::RGBA
		&&	(m_CompressionThreshold >= 0 && Image->GetWidth() * Image->GetHeight() > m_CompressionThreshold)
		)
	{
		texture = CreateCompressedTexture(Image, Key, texMemSize);
	}
	else
	{
		texture = CreateTexture(Image, texMemSize);
	}

	if(texture)
	{
		const uint32 textureId = s_NextTextureId++;
		STexture *texData = new STexture(textureId, TextureUrl, texture, texMemSize);
		if(texData != 0)
		{
			m_CachedTextures.Add(textureId, texData);
			m_TexturesByUrl.Add(TextureUrl, texData);
		}
	}
}

GenericTwin::TextureProxyPtr UGenericTwinTextureCache::GetTexture(const FString &TextureUrl) const
{
	if(m_TexturesByUrl.Contains(TextureUrl))
	{
		STexture *texture = m_TexturesByUrl[TextureUrl];
		texture->RefCount++;
		return GenericTwin::TextureProxyPtr(new GenericTwin::STextureProxy(texture->Id, texture->Texture2D, texture->MemorySize));
	}

	return GenericTwin::TextureProxyPtr();
}

GenericTwin::TextureProxyPtr UGenericTwinTextureCache::CreateTextureArray(int32 Width, int32 Height, int32 Depth, EPixelFormat Format)
{
	UTexture2DArray *texArray = UTexture2DArray::CreateTransient(Width, Height, Depth, Format);
	if(texArray)
	{
		m_Textures.Add(texArray);

		const uint32 textureId = s_NextTextureId++;

		STexture *texture = new STexture(textureId, texArray, Width * Height * Depth /* * NumElements(Format) */);
		if(texture)
		{
			texture->RefCount++;
			m_CachedTextures.Add(textureId, texture);
			return GenericTwin::TextureProxyPtr(new GenericTwin::STextureProxy(textureId, texArray, texture->MemorySize));
		}
	}

	return GenericTwin::TextureProxyPtr();
}

void UGenericTwinTextureCache::ReleaseTexture(uint32 TextureId)
{
	if(m_CachedTextures.Contains(TextureId))
	{
		STexture *texData = m_CachedTextures[TextureId];
		texData->RefCount--;

		if(texData->RefCount <= 0)
		{
			DestroyTexture(*texData);
		}
	}
}

UTexture2D* UGenericTwinTextureCache::CreateTexture(const GenericTwin::SImagePtr &Image, uint32 &TextureMemorySize)
{
	FTaskTagScope taskScope(ETaskTag::EParallelRenderingThread);

	UTexture2D *texture = UTexture2D::CreateTransient(Image->GetWidth(), Image->GetHeight(), PF_R8G8B8A8);

	if (texture)
	{
		m_Textures.Add(texture);

		void *textureData = texture->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
		FMemory::Memcpy(textureData, Image->GetData(), Image->GetDataSize());
		texture->GetPlatformData()->Mips[0].BulkData.Unlock();

		texture->UpdateResource();
		TextureMemorySize = static_cast<uint32> (Image->GetDataSize());

		UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Texture created"));
	}
	else
	{
		UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Couldn't create texture"));
	}

	return texture;
}

UTexture2D* UGenericTwinTextureCache::CreateCompressedTexture(const GenericTwin::SImagePtr &Image, const GenericTwin::FCacheKey &Key, uint32 &TextureMemorySize)
{
	UTexture2D* texture = 0;

	FIntVector2 alphaRange = Image->GetAlphaRange();

	int32 compressedSize = 0;

	const uint8 *compressed = 0;
	EPixelFormat format = EPixelFormat::PF_Unknown;

	if(alphaRange.X == 255)
	{
		compressed = GenericTwin::TextureCompression::CompressDXT1(Image, compressedSize);
		format = EPixelFormat::PF_DXT1;
	}
	else
	{
		compressed = GenericTwin::TextureCompression::CompressDXT5(Image, compressedSize);
		format = EPixelFormat::PF_DXT5;
	}

	if(compressed)
	{
		FTaskTagScope taskScope(ETaskTag::EParallelRenderingThread);
		texture = UTexture2D::CreateTransient(Image->GetWidth(), Image->GetHeight(), format);

		if (texture)
		{
			m_Textures.Add(texture);

			void *textureData = texture->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(textureData, compressed, compressedSize);
			texture->GetPlatformData()->Mips[0].BulkData.Unlock();

			texture->UpdateResource();
			TextureMemorySize = static_cast<uint32> (compressedSize);

			UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Compressed Texture created"));
		}
		else
		{
			UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Couldn't create compressed texture"));
		}

		GenericTwin::LocalFileCache &cache = GenericTwin::LocalFileCache::GetInstance();
		if(cache.IsInitialized())
		{
			SCacheTexture cachedTexture(Image->GetWidth(), Image->GetHeight(), format, compressedSize);
			if(cache.BeginFile(Key, &cachedTexture, offsetof(SCacheTexture, Data)))
			{
				cache.AddToFile(Key, compressed, compressedSize);
			}
		}

		FMemory::Free(const_cast<uint8*> (compressed));
	}

	return texture;
}

UTexture2D* UGenericTwinTextureCache::CreateTextureFromCache(int32 Width, int32 Height, EPixelFormat Format, int32 DataSize, const void *Data)
{
	FTaskTagScope taskScope(ETaskTag::EParallelRenderingThread);
	UTexture2D* texture = UTexture2D::CreateTransient(Width, Height, Format);

	if (texture)
	{
		m_Textures.Add(texture);
		
		void *textureData = texture->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
		FMemory::Memcpy(textureData, Data, DataSize);
		texture->GetPlatformData()->Mips[0].BulkData.Unlock();

		texture->UpdateResource();

		UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Texture created from cache"));
	}
	else
	{
		UE_LOG(LogGenericTwinTextureCache, Log, TEXT("Couldn't create cached texture"));
	}

	return texture;
}

void UGenericTwinTextureCache::DestroyTexture(STexture &TextureData)
{
	UTexture *texture = 0;
	
	if(TextureData.Texture2D)
	{
		texture = TextureData.Texture2D;
		delete TextureData.Texture2D->GetPlatformData();
		TextureData.Texture2D->SetPlatformData(0);
	}
	else
	{
		texture = TextureData.Texture2DArray;
	}

	if(texture)
	{
		m_Textures.Remove(texture);
		texture->RemoveFromRoot();
		texture->ConditionalBeginDestroy();
	}
}

UTexture2D* UGenericTwinTextureCache::CreateDummyTexture(int32 size)
{
	FTaskTagScope taskScope(ETaskTag::EParallelRenderingThread);
	UTexture2D* texture = UTexture2D::CreateTransient(size, size, PF_R8G8B8A8);

	if (texture)
	{
		TArray<uint8> img;

		for(int32 i = 0; i < (size * size); ++i)
		{
			img.Add(255);
			img.Add(0);
			img.Add(255);
			img.Add(255);
		}

		void *textureData = texture->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
		FMemory::Memcpy(textureData, img.GetData(), img.Num());
		texture->GetPlatformData()->Mips[0].BulkData.Unlock();

		texture->UpdateResource();

	}
	return texture;
}

void UGenericTwinTextureCache::CreateDummyTexture()
{
	if(m_DummyTexture == 0)
	{
		m_DummyTexture = CreateDummyTexture(16);
	}

}
