/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Common/Material/GenericTwinMaterialManager.h"
#include "Common/Material/MaterialMappingItem.h"

#include "Common/Material/GenericTwinTextureCache.h"
#include "Common/Material/MaterialProxy.h"
#include "Common/Material/TextureProxy.h"

#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"

DEFINE_LOG_CATEGORY(LogGenericTwinMaterialManager);

void UGenericTwinMaterialManager::Shutdown()
{
	m_MaterialRefCountMap.Empty();
}

UGenericTwinMaterialManager::~UGenericTwinMaterialManager()
{
}

void UGenericTwinMaterialManager::LoadGenericMapping(const FString &MaterialMappingFile)
{
	FString jsonStr;
	if (FFileHelper::LoadFileToString(jsonStr, *MaterialMappingFile))
	{
		UE_LOG(LogGenericTwinMaterialManager, Log, TEXT("Successfully opened material mapping file %s"), *MaterialMappingFile);
		TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(jsonStr);

		TSharedPtr<FJsonObject> jsonObject;
		if (FJsonSerializer::Deserialize(reader, jsonObject))
		{
			UE_LOG(LogGenericTwinMaterialManager, Log, TEXT("Successfully deserialized material mapping config file"));

			const TSharedPtr<FJsonObject> *defaultMatObject = 0;
			if	(	jsonObject->TryGetObjectField(TEXT("default_material"), defaultMatObject)
				&&	defaultMatObject
				&&	(*defaultMatObject)->HasTypedField<EJson::String>(TEXT("material"))
				)
			{
				m_DefaultMaterialItem.MaterialReference = (*defaultMatObject)->GetStringField(TEXT("material"));
				ParseParameters(*defaultMatObject, m_DefaultMaterialItem);

			}

			const TArray < TSharedPtr < FJsonValue > > *mappings = 0;
			if	(	jsonObject->TryGetArrayField(TEXT("mappings"), mappings)
				&&	mappings
				)
			{
				for(const auto &mapping : (*mappings))
				{
					GenericTwin::SMaterialMappingItem matItem;
					const TSharedPtr < FJsonObject > *mappingObject = 0;
					if	(	mapping->TryGetObject(mappingObject)
						&&	mappingObject
						&&	ParseMaterialItem(*mappingObject, matItem)
						)
					{
						m_GenericMaterialMap.Add(matItem);
					}
				}
			}

			UE_LOG(LogGenericTwinMaterialManager, Log, TEXT("Successfully extracted %d material mappings"), m_GenericMaterialMap.Num() );

		}
		else
			UE_LOG(LogGenericTwinMaterialManager, Error, TEXT("Not a valid material mapping file"));
	}
	else
		UE_LOG(LogGenericTwinMaterialManager, Error, TEXT("Couldn't open material mapping file %s"), *MaterialMappingFile);
}

// void UGenericTwinMaterialManager::ClearExactMaterialMapping()
// {
// 	m_ExactMaterialMap.Empty();
// }

// void UGenericTwinMaterialManager::SetExactMaterialMapping(const TMap<FString, GenericTwin::SMaterialMappingItem> &ExactMaterialMap)
// {
// 	m_ExactMaterialMap = ExactMaterialMap;
// }

void UGenericTwinMaterialManager::ReleaseMaterial(UMaterialInterface *Material)
{
	if(m_MaterialRefCountMap.Contains(Material))
	{
		MaterialInstancePtr &matInstPtr = m_MaterialRefCountMap[Material];
		--matInstPtr->RefCount;
		if(matInstPtr->RefCount == 0)
		{
			m_MaterialRefCountMap.Remove(Material);
			m_CreatedMaterials.Remove(Material);
		}
	}
}

GenericTwin::MaterialProxyPtr  UGenericTwinMaterialManager::GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial, const FString &MaterialReference)
{
	MaterialInstancePtr matInstPtr = CreateMaterialInstance(MaterialReference);
	if	(	matInstPtr
		&&	matInstPtr->IsValid()
		)
	{
		return GenericTwin::MaterialProxyPtr(new GenericTwin::SMaterialProxy(matInstPtr->Instance, ConfigureMaterialInstance(matInstPtr, SceneMaterial)) );
	}
	return GenericTwin::MaterialProxyPtr();
}

GenericTwin::MaterialProxyPtr UGenericTwinMaterialManager::GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial, TMap<FString, GenericTwin::SMaterialMappingItem> &ExactMaterialMap)
{
	UMaterialInterface *material = 0;
	uint32 texMemSize = 0;
	const FString &MaterialName = SceneMaterial.MaterialName;
	FString lowerMatName = MaterialName.ToLower();
	if(ExactMaterialMap.Contains(lowerMatName))
	{
		if(ExactMaterialMap[lowerMatName].Material == 0)
		{
			MaterialInstancePtr matInstPtr = CreateMaterialInstance(ExactMaterialMap[lowerMatName].MaterialReference);
			if	(	matInstPtr
				&&	matInstPtr->IsValid()
				)
			{
				texMemSize = ConfigureMaterialInstance(matInstPtr, ExactMaterialMap[MaterialName]);
				ExactMaterialMap[lowerMatName].Material = matInstPtr->Instance;
				material = matInstPtr->Instance;
			}
		}
		else
		{
			material = ExactMaterialMap[lowerMatName].Material;
			if(m_MaterialRefCountMap.Contains(material))
				m_MaterialRefCountMap[material]->RefCount += 1;
		}
	}
	else
	{
		for(GenericTwin::SMaterialMappingItem &matItem : m_GenericMaterialMap)
		{
			bool found = false;
			for(const FString &keyword : matItem.Keywords)
			{
				if(lowerMatName.Contains(keyword, ESearchCase::CaseSensitive, ESearchDir::FromStart))
				{
					found = true;
					break;
				}
			}
			if(found)
			{
				if(matItem.Material == 0)
				{
					MaterialInstancePtr matInstPtr = CreateMaterialInstance(matItem.MaterialReference);
					if	(	matInstPtr
						&&	matInstPtr->IsValid()
						)
					{
						texMemSize = ConfigureMaterialInstance(matInstPtr, matItem);
						matItem.Material = matInstPtr->Instance;
					}
				}
				material = matItem.Material;
				break;
			}
			else if(m_DefaultMaterialItem.MaterialReference.IsEmpty() == false)
			{
				MaterialInstancePtr matInstPtr = CreateMaterialInstance(m_DefaultMaterialItem.MaterialReference);
				if	(	matInstPtr
					&&	matInstPtr->IsValid()
					)
				{
					texMemSize = ConfigureMaterialInstance(matInstPtr, m_DefaultMaterialItem);

					texMemSize += ConfigureMaterialInstance(matInstPtr, SceneMaterial);

					material = matInstPtr->Instance;
				}
			}
		}
	}

	return GenericTwin::MaterialProxyPtr(new GenericTwin::SMaterialProxy(material, texMemSize));
}

// GenericTwin::MaterialProxyPtr UGenericTwinMaterialManager::GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial)
// {
// 	UMaterialInterface *material = 0;
// 	uint32 texMemSize = 0;
// 	const FString &MaterialName = SceneMaterial.MaterialName;
// 	FString lowerMatName = MaterialName.ToLower();
// 	if(m_ExactMaterialMap.Contains(lowerMatName))
// 	{
// 		if(m_ExactMaterialMap[lowerMatName].Material == 0)
// 		{
// 			MaterialInstancePtr matInstPtr = CreateMaterialInstance(m_ExactMaterialMap[lowerMatName].MaterialReference);
// 			if	(	matInstPtr
// 				&&	matInstPtr->IsValid()
// 				)
// 			{
// 				texMemSize = ConfigureMaterialInstance(matInstPtr, m_ExactMaterialMap[MaterialName]);
// 				m_ExactMaterialMap[lowerMatName].Material = matInstPtr->Instance;
// 				material = matInstPtr->Instance;
// 			}
// 		}
// 		else
// 		{
// 			material = m_ExactMaterialMap[lowerMatName].Material;
// 			if(m_MaterialRefCountMap.Contains(material))
// 				m_MaterialRefCountMap[material]->RefCount += 1;
// 		}
// 	}
// 	else
// 	{
// 		for(GenericTwin::SMaterialMappingItem &matItem : m_GenericMaterialMap)
// 		{
// 			bool found = false;
// 			for(const FString &keyword : matItem.Keywords)
// 			{
// 				if(lowerMatName.Contains(keyword, ESearchCase::CaseSensitive, ESearchDir::FromStart))
// 				{
// 					found = true;
// 					break;
// 				}
// 			}
// 			if(found)
// 			{
// 				if(matItem.Material == 0)
// 				{
// 					MaterialInstancePtr matInstPtr = CreateMaterialInstance(matItem.MaterialReference);
// 					if	(	matInstPtr
// 						&&	matInstPtr->IsValid()
// 						)
// 					{
// 						texMemSize = ConfigureMaterialInstance(matInstPtr, matItem);
// 						matItem.Material = matInstPtr->Instance;
// 					}
// 				}
// 				material = matItem.Material;
// 				break;
// 			}
// 			else if(m_DefaultMaterialItem.MaterialReference.IsEmpty() == false)
// 			{
// 				MaterialInstancePtr matInstPtr = CreateMaterialInstance(m_DefaultMaterialItem.MaterialReference);
// 				if	(	matInstPtr
// 					&&	matInstPtr->IsValid()
// 					)
// 				{
// 					texMemSize = ConfigureMaterialInstance(matInstPtr, m_DefaultMaterialItem);

// 					texMemSize += ConfigureMaterialInstance(matInstPtr, SceneMaterial);

// 					material = matInstPtr->Instance;
// 				}
// 			}
// 		}
// 	}

// 	return GenericTwin::MaterialProxyPtr(new GenericTwin::SMaterialProxy(material, texMemSize));
// }

bool UGenericTwinMaterialManager::ParseMaterialItem(const TSharedPtr<FJsonObject> &MappingObject, GenericTwin::SMaterialMappingItem &MaterialItem)
{
	bool res = false;
	if	(	MappingObject->HasTypedField<EJson::String>(TEXT("material"))
		&&	(	MappingObject->HasTypedField<EJson::Array>(TEXT("keywords"))
			||	MappingObject->HasTypedField<EJson::String>(TEXT("material_name"))
			)
		)
	{
		MaterialItem.MaterialReference = MappingObject->GetStringField(TEXT("material"));

		if(MappingObject->TryGetStringField(TEXT("material_name"), MaterialItem.MaterialName))
		{
			MaterialItem.MaterialName.ToLowerInline();
			res = true;
		}
		else
		{
			const TArray < TSharedPtr < FJsonValue > > *keywordsArray = 0;
			if	(	MappingObject->TryGetArrayField(TEXT("keywords"), keywordsArray)
				&&	keywordsArray
				)
			{
				for(const auto &keywordVal : (*keywordsArray))
				{
					if(keywordVal->Type == EJson::String)
						MaterialItem.Keywords.Add( keywordVal->AsString().ToLower() );
				}
			}
			res = true;
		}

		ParseParameters(MappingObject, MaterialItem);
	}
	return res;
}

void UGenericTwinMaterialManager::ParseParameters(const TSharedPtr<FJsonObject> &JsonObject, GenericTwin::SMaterialMappingItem &MaterialItem)
{
	const TSharedPtr<FJsonObject> *paramsObject = 0;
	if	(	JsonObject->TryGetObjectField(TEXT("parameters"), paramsObject)
		&&	paramsObject
		)
	{
		TArray<FString> keys;
		(*paramsObject)->Values.GetKeys(keys);
		for(const FString &key : keys)
		{
			const TSharedPtr< FJsonValue > &paramVal = (*paramsObject)->Values[key];
			if(paramVal->Type == EJson::Number)
			{
				MaterialItem.ScalarParameters.Add(key, paramVal->AsNumber());
			}
			else if(paramVal->Type == EJson::String)
			{
				MaterialItem.TextureParameters.Add(key, paramVal->AsString());
			}
			else if(paramVal->Type == EJson::Array)
			{
				const TArray< TSharedPtr< FJsonValue > > &vals = paramVal->AsArray();
				FLinearColor vectorParam(EForceInit::ForceInitToZero);
				if(vals.Num() > 0)
					vectorParam.R = vals[0]->Type == EJson::Number ? vals[0]->AsNumber() : 0.0;
				if(vals.Num() > 1)
					vectorParam.G = vals[1]->Type == EJson::Number ? vals[1]->AsNumber() : 0.0;
				if(vals.Num() > 2)
					vectorParam.B = vals[2]->Type == EJson::Number ? vals[2]->AsNumber() : 0.0;
				if(vals.Num() > 3)
					vectorParam.A = vals[3]->Type == EJson::Number ? vals[3]->AsNumber() : 0.0;

				MaterialItem.VectorParameters.Add(key, vectorParam);
			}
		}
	}
}

UGenericTwinMaterialManager::MaterialInstancePtr UGenericTwinMaterialManager::CreateMaterialInstance(const FString &MaterialReference)
{
	MaterialInstancePtr matInstPtr(new SMaterialInstance);

	UMaterialInterface *parentMaterial = m_ParentMaterials.Contains(MaterialReference) ? m_ParentMaterials[MaterialReference] : 0;

	if(parentMaterial == 0)
	{
		parentMaterial = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), this, *MaterialReference));
		if(parentMaterial == 0)
			parentMaterial = Cast<UMaterialInstance>(StaticLoadObject(UMaterialInstance::StaticClass(), nullptr, *MaterialReference));

		// also store null pointer, if loading failed this time it most likely fails all the time
		m_ParentMaterials.Add(MaterialReference, parentMaterial);
	
		if(parentMaterial == 0)
		{
			UE_LOG(LogGenericTwinMaterialManager, Warning, TEXT("Failed to load material (instance) %s, won't try again"), *MaterialReference);
		}
	}

	if(parentMaterial)
	{
		matInstPtr->Instance = UMaterialInstanceDynamic::Create(parentMaterial, GetWorld());
	}

	if(matInstPtr)
	{
		m_MaterialRefCountMap.Add(matInstPtr->Instance, matInstPtr);
		m_CreatedMaterials.Add(matInstPtr->Instance);
	}

	return matInstPtr;
}


uint32 UGenericTwinMaterialManager::ConfigureMaterialInstance(const MaterialInstancePtr &MaterialInstancePtr, const GenericTwin::SMaterialMappingItem &MaterialItem) const
{
	uint32 texMemSize = 0;
	for(const auto &item : MaterialItem.ScalarParameters)
	{
		MaterialInstancePtr->Instance->SetScalarParameterValue(*(item.Key), item.Value);
	}

	for(const auto &item : MaterialItem.VectorParameters)
	{
		MaterialInstancePtr->Instance->SetVectorParameterValue(*(item.Key), item.Value);
	}

	for(const auto &item : MaterialItem.TextureParameters)
	{
		// create and load texture
	}

	return texMemSize;
}

uint32  UGenericTwinMaterialManager::ConfigureMaterialInstance(const MaterialInstancePtr &MaterialInstancePtr, const GenericTwin::SSceneMaterial &SceneMaterial) const
{
	uint32 texMemSize = 0;

	for(const auto &item : SceneMaterial.Parameters)
	{
		MaterialInstancePtr->Instance->SetVectorParameterValue(*(item.Key), item.Value);
	}

	if(m_TextureCache)
	{
		for(const auto &item : SceneMaterial.Textures)
		{
			const FString &textureUrl = item.Value;
			GenericTwin::TextureProxyPtr texProxyPtr = m_TextureCache->GetTexture(textureUrl);

			if	(	texProxyPtr
				&&	texProxyPtr->IsValid()
				)
			{
				UTexture2D *texture = texProxyPtr->GetTexture2D();
				if(texture)
				{
					MaterialInstancePtr->Textures.Add(texProxyPtr);

					const FString &samplerName = item.Key;
					MaterialInstancePtr->Instance->SetTextureParameterValue(*samplerName, texture);

					texMemSize = texProxyPtr->GetTextureMemorySize();
				}
			}
		}
	}

	return texMemSize;
}
