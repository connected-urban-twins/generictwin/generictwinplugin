// /**The MIT License (MIT)Copyright (c) 2023 - present, Hamburg Port AuthorityPermission is hereby granted, free of charge, to any person obtaining a copyof this software and associated documentation files (the “Software”), to dealin the Software without restriction, including without limitation the rightsto use, copy, modify, merge, publish, distribute, sublicense, and/or sellcopies of the Software, and to permit persons to whom the Software isfurnished to do so, subject to the following conditions:The above copyright notice and this permission notice shall be included inall copies or substantial portions of the Software.THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS ORIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS INTHE SOFTWARE.**/

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Environment/GenericTwinEnvironmentTypes.h"
#include "GenericTwinEnvironmentControlCmp.generated.h"

class UGenericTwinEnvControlInterface;

UCLASS( ClassGroup=(Custom) )
class UGenericTwinEnvironmentControlCmp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGenericTwinEnvironmentControlCmp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	void Initialize(const TSharedPtr<FJsonObject> &JsonConfig, double Longitude, double Latitude);

	UFUNCTION(BlueprintCallable, Category = "GenericTwin|Environment")
	void SetDate(int32 Day, int32 Month, int32 Year);

	UFUNCTION(BlueprintCallable, Category = "GenericTwin|Environment")
	void SetSolarTime(float SolarTime);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GenericTwin|Environment")
	float GetSolarTime() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GenericTwin|Environment")
	TArray<EGenericTwinWeatherType> GetAvailableWeather();

	UFUNCTION(BlueprintCallable, Category = "GenericTwin|Environment")
	void SetWeather(EGenericTwinWeatherType WeatherType);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GenericTwin|Environment")
	EGenericTwinWeatherType GetWeather();

	// void SetGlobalWindDirection(const FVector &Direction);
	// GetGlobalWindDirection()
	// GetLocalWindDirection(Location)


private:

	UPROPERTY()
	TObjectPtr<AActor>									m_EnvironmentControl;
};
