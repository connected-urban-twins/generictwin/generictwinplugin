// /**The MIT License (MIT)Copyright (c) 2023 - present, Hamburg Port AuthorityPermission is hereby granted, free of charge, to any person obtaining a copyof this software and associated documentation files (the “Software”), to dealin the Software without restriction, including without limitation the rightsto use, copy, modify, merge, publish, distribute, sublicense, and/or sellcopies of the Software, and to permit persons to whom the Software isfurnished to do so, subject to the following conditions:The above copyright notice and this permission notice shall be included inall copies or substantial portions of the Software.THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS ORIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS INTHE SOFTWARE.**/


#include "Environment/GenericTwinEnvironmentControlCmp.h"
#include "Environment/GenericTwinEnvControlInterface.h"

#include "Kismet/GameplayStatics.h"

#include "Common/Configuration/JsonParser.h"
#include "Dom/JsonObject.h"

// Sets default values for this component's properties
UGenericTwinEnvironmentControlCmp::UGenericTwinEnvironmentControlCmp()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UGenericTwinEnvironmentControlCmp::BeginPlay()
{
	Super::BeginPlay();


}

void UGenericTwinEnvironmentControlCmp::Initialize(const TSharedPtr<FJsonObject> &JsonConfig, double Longitude, double Latitude)
{
	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsWithInterface(this, UGenericTwinEnvControlInterface::StaticClass(), actors);
	if(actors.IsEmpty() == false)
	{
		m_EnvironmentControl = actors[0];
	}

	if(m_EnvironmentControl)
	{
		EGenericTwinWeatherType weatherType = EGenericTwinWeatherType::PARTLY_CLOUDY;
		int32 day = 1;
		int32 month = 1;
		int32 year = 2000;
		float solarTime = 12.0f;
		if(JsonConfig->HasTypedField<EJson::Object>(TEXT("environment")))
		{
			const TSharedPtr< FJsonObject > &envRootObj = JsonConfig->GetObjectField(TEXT("environment"));

			GenericTwin::FJsonParser parser;

			TMap<FString, uint32> weatherTypeMap =	{	{TEXT("clear_sky"), static_cast<uint32> (EGenericTwinWeatherType::CLEAR_SKY)}
													,	{TEXT("partly_cloudy"), static_cast<uint32> (EGenericTwinWeatherType::PARTLY_CLOUDY)}
													,	{TEXT("cloudy"), static_cast<uint32> (EGenericTwinWeatherType::CLOUDY)}
													,	{TEXT("overcast"), static_cast<uint32> (EGenericTwinWeatherType::OVERCAST)}
													,	{TEXT("light_rain"), static_cast<uint32> (EGenericTwinWeatherType::LIGHT_RAIN)}
													,	{TEXT("rain"), static_cast<uint32> (EGenericTwinWeatherType::RAIN)}
													,	{TEXT("thunderstorm"), static_cast<uint32> (EGenericTwinWeatherType::THUNDERSTORM)}
													,	{TEXT("light_snow"), static_cast<uint32> (EGenericTwinWeatherType::LIGHT_SNOW)}
													,	{TEXT("snow"), static_cast<uint32> (EGenericTwinWeatherType::SNOW)}
													,	{TEXT("blizzard"), static_cast<uint32> (EGenericTwinWeatherType::BLIZZARD)}
													,	{TEXT("light_sandstorm"), static_cast<uint32> (EGenericTwinWeatherType::LIGHT_SANDSTORM)}
													,	{TEXT("sandstorm"), static_cast<uint32> (EGenericTwinWeatherType::SANDSTORM)}
													};


			parser.ParseObject	(	envRootObj
								,	{	{GenericTwin::EJsonDataType::Enumerator, TEXT("weather"), &weatherType, true, weatherTypeMap}
									,	{GenericTwin::EJsonDataType::Float, TEXT("solar_time"), &solarTime, true}
									}
								);

			if(envRootObj->HasTypedField<EJson::Object>(TEXT("date")))
			{
				const TSharedPtr<FJsonObject> &dateObj = envRootObj->GetObjectField(TEXT("date"));
				parser.ParseObject	(	dateObj
									,	{	{GenericTwin::EJsonDataType::Integer, TEXT("day"), &day, true}
										,	{GenericTwin::EJsonDataType::Integer, TEXT("month"), &month, true}
										,	{GenericTwin::EJsonDataType::Integer, TEXT("year"), &year, true}
										}
									);

			}
		}


		IGenericTwinEnvControlInterface::Execute_SetLocation(m_EnvironmentControl, Longitude, Latitude);
		IGenericTwinEnvControlInterface::Execute_SetDate(m_EnvironmentControl, day, month, year);
		IGenericTwinEnvControlInterface::Execute_SetSolarTime(m_EnvironmentControl, solarTime);
		IGenericTwinEnvControlInterface::Execute_SetWeather(m_EnvironmentControl, weatherType);
	}
}


void UGenericTwinEnvironmentControlCmp::SetDate(int32 Day, int32 Month, int32 Year)
{
	if(m_EnvironmentControl)
	{
		IGenericTwinEnvControlInterface::Execute_SetDate(m_EnvironmentControl, Day, Month, Year);
	}
}

void UGenericTwinEnvironmentControlCmp::SetSolarTime(float SolarTime)
{
	if(m_EnvironmentControl)
	{
		IGenericTwinEnvControlInterface::Execute_SetSolarTime(m_EnvironmentControl, SolarTime);
	}
}


float UGenericTwinEnvironmentControlCmp::GetSolarTime() const
{
	return		m_EnvironmentControl
			?	IGenericTwinEnvControlInterface::Execute_GetSolarTime(m_EnvironmentControl)
			:	0.0f
			;
}

TArray<EGenericTwinWeatherType> UGenericTwinEnvironmentControlCmp::GetAvailableWeather()
{
	return		m_EnvironmentControl
			?	IGenericTwinEnvControlInterface::Execute_GetAvailableWeather(m_EnvironmentControl)
			:	TArray<EGenericTwinWeatherType>()
			;
}

void UGenericTwinEnvironmentControlCmp::SetWeather(EGenericTwinWeatherType WeatherType)
{
	if(m_EnvironmentControl)
	{
		IGenericTwinEnvControlInterface::Execute_SetWeather(m_EnvironmentControl, WeatherType);
	}
}

EGenericTwinWeatherType UGenericTwinEnvironmentControlCmp::GetWeather()
{
	return		m_EnvironmentControl
			?	IGenericTwinEnvControlInterface::Execute_GetWeather(m_EnvironmentControl)
			:	EGenericTwinWeatherType::UNDEFINED
			;
}
