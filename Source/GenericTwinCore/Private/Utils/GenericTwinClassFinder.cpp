/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Utils/GenericTwinClassFinder.h"

UGenericTwinClassFinder* UGenericTwinClassFinder::s_Instance = 0;

UGenericTwinClassFinder::UGenericTwinClassFinder()
{
}

void UGenericTwinClassFinder::Destroy()
{
	if (s_Instance)
		s_Instance->m_Self = 0;
	// delete s_Instance;
	// s_Instance = 0;
}

UClass* UGenericTwinClassFinder::FindClass(UWorld *World, const FString &Reference)
{
	if(s_Instance == nullptr)
	{
		s_Instance = NewObject<UGenericTwinClassFinder>(World);
		if(s_Instance)
			s_Instance->m_Self = s_Instance;
	}

	UClass *foundClass = 0;
	if(s_Instance)
	{
		// if(s_Instance->m_ClassMap.Contains(Reference))
		// {
		// 	foundClass = s_Instance->m_ClassMap[Reference];
		// }
		// else
		{
			foundClass = s_Instance->FindClass(Reference);
			if(foundClass)
			{
				// s_Instance->m_ClassMap.Add(Reference, foundClass);
			}
		}
	}

	return foundClass;
}


UClass* UGenericTwinClassFinder::FindClass(const FString &Reference)
{
	FString PathName(Reference);
	int32 PackageDelimPos = INDEX_NONE;
	PathName.FindChar(TCHAR('.'), PackageDelimPos);
	if (PackageDelimPos == INDEX_NONE)
	{
		int32 ObjectNameStart = INDEX_NONE;
		PathName.FindLastChar(TCHAR('/'), ObjectNameStart);
		if (ObjectNameStart != INDEX_NONE)
		{
			const FString ObjectName = PathName.Mid(ObjectNameStart + 1);
			PathName += TCHAR('.');
			PathName += ObjectName;
			PathName += TCHAR('_');
			PathName += TCHAR('C');
		}
	}

	UClass *classPtr = StaticLoadClass(AActor::StaticClass(), NULL, *PathName);
	if (classPtr)
	{
		classPtr->AddToRoot();
	}

	return classPtr;
}
