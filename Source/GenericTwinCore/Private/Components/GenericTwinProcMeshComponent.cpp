/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Components/GenericTwinProcMeshComponent.h"
#include "Common/BaseTypes.h"

void UGenericTwinProcMeshComponent::Construct(AActor *Actor, EUploadMode UploadMode)
{
}

void UGenericTwinProcMeshComponent::Initialize(GenericTwin::IStaticMeshComponent::EUploadMode UploadMode)
{
}

void UGenericTwinProcMeshComponent::Clear()
{
	if(IsInGameThread())
	{
		_Clear();
	}

}

void UGenericTwinProcMeshComponent::_Clear()
{
	ClearAllMeshSections();
	m_nextSectionIndex = 0;
}

int32 UGenericTwinProcMeshComponent::AddMeshSection(const GenericTwin::SMeshSection& MeshSection, UMaterialInterface* Material)
{
	// TArray< FVector >			Vertices;
	// TArray< FVector >			Normals;
	// TArray< FVector2D >			UV0;
	// TArray< FVector2D >			UV1;
	// TArray< FLinearColor >		Colors;
	// TArray< int32 >				Indices;

	// UMaterialInterface			*Material = 0;

	if(MeshSection.UV1.IsEmpty())
	{
		CreateMeshSection(m_nextSectionIndex, MeshSection.Vertices, MeshSection.Indices, MeshSection.Normals, MeshSection.UV0, TArray<FColor>(), TArray<FProcMeshTangent>(), true );
	}
	else
	{
		CreateMeshSection(m_nextSectionIndex, MeshSection.Vertices, MeshSection.Indices, MeshSection.Normals, MeshSection.UV0, MeshSection.UV1, TArray<FVector2D>(), TArray<FVector2D>(), TArray<FColor>(), TArray<FProcMeshTangent>(), true );
	}

	SetMaterial(m_nextSectionIndex, Material);

	const int32 index = m_nextSectionIndex++;

	return index;
}

int32 UGenericTwinProcMeshComponent::AddMeshSection(const TArray<int32> &Indices, const TArray<FVector> &Vertices, const TArray<FVector> *Normals, const TArray<FVector2D> *UV0, const TArray<FVector2D> *UV1, const TArray<FLinearColor> *Colors, UMaterialInterface *Material)
{
	TArray<FVector2D> emptyUV;
	CreateMeshSection(m_nextSectionIndex, Vertices, Indices, Normals != nullptr ? *Normals :  TArray<FVector>(), UV0 != nullptr ? *UV0 : emptyUV, UV1 != nullptr ? *UV1 : emptyUV, emptyUV, emptyUV, TArray<FColor>(), TArray<FProcMeshTangent>(), true );
	SetMaterial(m_nextSectionIndex, Material);

	const int32 index = m_nextSectionIndex++;
	return index;
}

int32 UGenericTwinProcMeshComponent::AddMeshSections(const TArray<const GenericTwin::SMeshSection*>& MeshSections, const TArray<UMaterialInterface*>& Materials)
{
	return 0;
}

USceneComponent* UGenericTwinProcMeshComponent::GetSceneComponent()
{
	return this;
}

