/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Common/BaseTypes.h"
#include "Components/GenericTwinStaticMeshComponent.h"

#include "GenericTwinStaticMeshActor.generated.h"

namespace GenericTwin {
struct SMeshSection;
}	//	namespace GenericTwin


UCLASS()
class GENERICTWINCORE_API AGenericTwinStaticMeshActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGenericTwinStaticMeshActor();

	GenericTwin::IStaticMeshComponent::EUploadMode GetUploadMode() const;

	void Initialize();

	void Release();

	void ClearMesh();

	void AddMeshSection(const GenericTwin::SMeshSection &MeshSection);

	void AddMeshSection(const GenericTwin::SMeshSection &MeshSection, UMaterialInterface *Material);

	void SetFlattenedScene(const GenericTwin::ScenePtr &ScenePtr);

	void AddTextureProxy(const GenericTwin::TextureProxyPtr &TexProxyPtr);

	uint32 GetEstimatedGPUMemoryUsage() const;

	FMeshDescription GetMeshDescription();

	TSet<TObjectPtr<UMaterialInterface>> GetMaterials() const;

protected:

	UPROPERTY()
	USceneComponent									*Root = 0;

	GenericTwin::IStaticMeshComponent				*MeshComponent = 0;

	TArray<GenericTwin::MaterialProxyPtr>			m_Materials;

	TArray<GenericTwin::TextureProxyPtr>			m_AdditionalTextures;

	int32											m_nextMeshSectionIndex = 0;

	uint32											m_EstimatedGPUMemoryUsage = 0;
};


inline uint32 AGenericTwinStaticMeshActor::GetEstimatedGPUMemoryUsage() const
{
	return m_EstimatedGPUMemoryUsage;
}

inline GenericTwin::IStaticMeshComponent::EUploadMode AGenericTwinStaticMeshActor::GetUploadMode() const
{
	return MeshComponent->GetUploadMode();
}
