/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Info.h"
#include "TwinWorldContext.h"
#include "Common/Configuration/GenericConfiguration.h"
#include "GenericTwin.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogGenericTwin, Log, All);


namespace GenericTwin {

class LocalFileCache;

}	//	namespace GenericTwin


class UGenericTwinEnvironmentControlCmp;

/**
 * 
 */
UCLASS()
class GENERICTWINCORE_API AGenericTwin : public AInfo
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "GenericTwin", meta = (WorldContext = "WorldContextObject"))
	static AGenericTwin* GetGenericTwin(UObject* WorldContextObject);

	AGenericTwin();
	virtual ~AGenericTwin();

public:

	virtual void Tick(float DeltaTime) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable)
	void Initialize(FString ConfigurationFile);

	FTwinWorldContext& GetWorldContext();
	const FTwinWorldContext& GetWorldContext() const;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UGenericTwinEnvironmentControlCmp>		EnvironmentControl;

private:

	FSceneView* getSceneView(APawn &ViewPawn);

	GenericConfiguration								m_Configuration;

	UPROPERTY()
	FTwinWorldContext									m_WorldContext;

};

inline FTwinWorldContext& AGenericTwin::GetWorldContext()
{
	return m_WorldContext;
}

inline const FTwinWorldContext& AGenericTwin::GetWorldContext() const
{
	return m_WorldContext;
}

