// /**The MIT License (MIT)Copyright (c) 2023 - present, Hamburg Port AuthorityPermission is hereby granted, free of charge, to any person obtaining a copyof this software and associated documentation files (the “Software”), to dealin the Software without restriction, including without limitation the rightsto use, copy, modify, merge, publish, distribute, sublicense, and/or sellcopies of the Software, and to permit persons to whom the Software isfurnished to do so, subject to the following conditions:The above copyright notice and this permission notice shall be included inall copies or substantial portions of the Software.THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS ORIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS INTHE SOFTWARE.**/

#pragma once

#include "Common/BaseTypes.h"
#include "Common/Base/SingletonManager.h"

#include "UObject/NoExportTypes.h"

#include "GenericTwinTextureCache.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogGenericTwinTextureCache, Log, All);

namespace GenericTwin {

class LocalFileCache;
class FCacheKey;

}	//	namespace GenericTwin

/**
 * 
 */
UCLASS()
class GENERICTWINCORE_API UGenericTwinTextureCache	:	public UObject
													,	public GenericTwin::TSingletonManager<class UGenericTwinTextureCache>
{
	GENERATED_BODY()

	friend class GenericTwin::TSingletonManager<class UGenericTwinTextureCache>;
	friend struct GenericTwin::STextureProxy;

private:

	struct SCacheTexture
	{
		SCacheTexture(int32 _Width, int32 _Height, EPixelFormat _Format, int32 _DataSize)
			:	Width(_Width)
			,	Height(_Height)
			,	Format(_Format)
			,	DataSize(_DataSize)
		{
		}

		int32					Width = 0;
		int32					Height = 0;
		EPixelFormat			Format;

		int32					DataSize = 0;
		const uint8				Data[1] = { 0 };
	};

	struct STexture
	{
		STexture(uint32 TextureId, const FString &TextureUrl, UTexture2D *Texture, uint32 TextureMemSize)
			:	Id(TextureId)
			,	Url(TextureUrl)
			,	Texture2D(Texture)
			,	MemorySize(TextureMemSize)
		{
		}

		STexture(uint32 TextureId, UTexture2DArray *TextureArray, uint32 TextureMemSize)
			:	Id(TextureId)
			,	Texture2DArray(TextureArray)
			,	MemorySize(TextureMemSize)
		{
		}

		uint32					Id = 0;

		int32					RefCount = 0;

		FString					Url;
		UTexture2D				*Texture2D = 0;
		UTexture2DArray			*Texture2DArray = 0;
	
		uint32					MemorySize = 0;
	};

public:

	virtual ~UGenericTwinTextureCache();

	bool AddTexture(const FString &TextureUrl);

	bool AddTexture(const FString &TextureUrl, const void *DataPtr, uint32 DataSize);

	GenericTwin::TextureProxyPtr GetTexture(const FString &TextureUrl) const;

	GenericTwin::TextureProxyPtr CreateTextureArray(int32 Width, int32 Height, int32 Depth, EPixelFormat Format);

	void SetMaxTextureSize(int32 MaxTextureSize);
	void SetCompressionThreshold(int32 CompressionThreshold);

private:

	UGenericTwinTextureCache();

	virtual void Shutdown() override;

	void AddTexture(const FString &TextureUrl, const GenericTwin::SImagePtr &Image, const GenericTwin::FCacheKey &Key);

	void ReleaseTexture(uint32 TextureId);

	UTexture2D* CreateTexture(const GenericTwin::SImagePtr &Image, uint32 &TextureMemoprySize);

	UTexture2D* CreateCompressedTexture(const GenericTwin::SImagePtr &Image, const GenericTwin::FCacheKey &Key, uint32 &TextureMemorySize);

	UTexture2D* CreateTextureFromCache(int32 _Width, int32 _Height, EPixelFormat _Format, int32 _DataSize, const void *Data);

	void DestroyTexture(STexture &TextureData);

	UTexture2D* CreateDummyTexture(int32 size);
	void CreateDummyTexture();

	SCacheTexture* compressDXT1(const GenericTwin::SImagePtr &Image);

	// keep textures at least referenced ones
	UPROPERTY()
	TSet<UTexture*>							m_Textures;

	TMap<int32, STexture*>					m_CachedTextures;

	TMap<FString, STexture*>				m_TexturesByUrl;

	int32									m_MaxTextureSize = 0;
	int32									m_CompressionThreshold = -1;

	UPROPERTY()
	UTexture2D								*m_DummyTexture = 0;

	static uint32							s_NextTextureId;

};

inline void UGenericTwinTextureCache::SetMaxTextureSize(int32 MaxTextureSize)
{
	m_MaxTextureSize = MaxTextureSize;
}

inline void UGenericTwinTextureCache::SetCompressionThreshold(int32 CompressionThreshold)
{
	m_CompressionThreshold = CompressionThreshold;
}
