/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "CoreMinimal.h"

#include "Common/Material/GenericTwinMaterialManager.h"

namespace GenericTwin {
struct GENERICTWINCORE_API SMaterialProxy
{
public:

	SMaterialProxy(UMaterialInterface *_Material = 0, uint32 TexMemSize = 0)
		:	Material(_Material)
		,	TextureMemorySize(TexMemSize)
	{
	}

	~SMaterialProxy()
	{
		if(Material)
			UGenericTwinMaterialManager::GetInstance().ReleaseMaterial(Material);
	}

	bool IsValid() const
	{
		return Material != 0;
	}

	UMaterialInterface* GetMaterial() const
	{
		return Material;
	}

	uint32 GetTextureMemorySize() const
	{
		return TextureMemorySize;
	}

private:

	UMaterialInterface		*Material = 0;

	uint32					TextureMemorySize = 0;
};

}	//	namespace GenericTwin
