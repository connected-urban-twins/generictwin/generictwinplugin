/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Common/BaseTypes.h"

#include "Common/Base/SingletonManager.h"

#include "Common/Material/MaterialMappingItem.h"

#include "GenericTwinMaterialManager.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogGenericTwinMaterialManager, Log, All);

namespace GenericTwin {

struct SSceneMaterial;

struct SMaterialProxy;

}	//	namespace GenericTwin

class UGenericTwinTextureCache;

/**
 * 
 */
UCLASS()
class GENERICTWINCORE_API UGenericTwinMaterialManager	:	public UObject
														,	public GenericTwin::TSingletonManager<class UGenericTwinMaterialManager>
{
	GENERATED_BODY()

	friend class GenericTwin::TSingletonManager<class UGenericTwinMaterialManager>;

	struct SMaterialInstance
	{
		SMaterialInstance(UMaterialInstanceDynamic * _Instance = 0)
			:	Instance(_Instance)
		{
		}

		bool IsValid() const
		{
			return Instance != 0;
		}

		int32									RefCount = 1;
		UMaterialInstanceDynamic				*Instance = 0;
		TArray<GenericTwin::TextureProxyPtr>	Textures;
	};

	typedef TSharedPtr<SMaterialInstance>	MaterialInstancePtr;

public:

	virtual ~UGenericTwinMaterialManager();

	void SetTextureCache(UGenericTwinTextureCache *TextureCache);

	void LoadGenericMapping(const FString &MaterialMappingFile);

	// void SetExactMaterialMapping(const TMap<FString, GenericTwin::SMaterialMappingItem> &ExactMaterialMap);
	// void ClearExactMaterialMapping();

	GenericTwin::MaterialProxyPtr GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial, const FString &MaterialReference);
	GenericTwin::MaterialProxyPtr GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial, TMap<FString, GenericTwin::SMaterialMappingItem> &ExactMaterialMap);
	
	// GenericTwin::MaterialProxyPtr GetMaterial(const GenericTwin::SSceneMaterial &SceneMaterial);

	void ReleaseMaterial(UMaterialInterface *Material);

	bool ParseMaterialItem(const TSharedPtr<FJsonObject> &MappingObject, GenericTwin::SMaterialMappingItem &MaterialItem);

private:

	virtual void Shutdown() override;

	void ParseParameters(const TSharedPtr<FJsonObject> &JsonObject, GenericTwin::SMaterialMappingItem &MaterialItem);

	MaterialInstancePtr CreateMaterialInstance(const FString &MaterialReference);

	uint32 ConfigureMaterialInstance(const MaterialInstancePtr &MaterialInstancePtr, const GenericTwin::SMaterialMappingItem &MaterialItem) const;
	uint32 ConfigureMaterialInstance(const MaterialInstancePtr &MaterialInstancePtr, const GenericTwin::SSceneMaterial &SceneMaterial) const;

	UGenericTwinTextureCache								*m_TextureCache = 0;

	TArray<GenericTwin::SMaterialMappingItem>				m_GenericMaterialMap;

	// TMap<FString, GenericTwin::SMaterialMappingItem>		m_ExactMaterialMap;

	GenericTwin::SMaterialMappingItem						m_DefaultMaterialItem;

	UPROPERTY()
	UMaterialInterface										*m_DefaultMaterial = 0;

	UPROPERTY()
	TMap<FString, UMaterialInterface*>						m_ParentMaterials;

	TMap<UMaterialInterface*, MaterialInstancePtr>			m_MaterialRefCountMap;

	UPROPERTY()
	TSet<UMaterialInterface*>								m_CreatedMaterials;

};


inline void UGenericTwinMaterialManager::SetTextureCache(UGenericTwinTextureCache *TextureCache)
{
	m_TextureCache = TextureCache;
}
