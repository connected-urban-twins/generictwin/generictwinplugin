/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "GenericTwinPath.h"

void UGenericTwinPath::AddFloatChannel(const FName &ChannelId, float InvalidValue)
{
	m_FloatChannels.Add(ChannelId, InvalidValue);
}

void UGenericTwinPath::AddWaypoint(const FTransform &Transform)
{
	SWaypoint waypoint;
	waypoint.Transform = Transform;
	m_Waypoints.Add(waypoint);
}


void UGenericTwinPath::BeginWaypoint()
{
	m_curWaypoint = SWaypoint();
	m_curWaypoint.FloatChannels = m_FloatChannels;
}

void UGenericTwinPath::SetFloatValue(const FName &ChannelId, float Value)
{
	if(m_FloatChannels.Contains(ChannelId))
	{
		m_curWaypoint.FloatChannels[ChannelId] = Value;
	}
}

void UGenericTwinPath::StoreWaypoint(const FTransform &Transform)
{
	m_curWaypoint.Transform = Transform;
	m_Waypoints.Add(m_curWaypoint);
}

void UGenericTwinPath::StartTraversal(float Speed, FOnAdvanceOnPath OnAdvanceOnPath, FOnTraversalFinished OnTraversalFinished)
{
	if((m_isActive = BuildSpline()) == true)
	{
		m_OnAdvanceOnPath = OnAdvanceOnPath;
		m_OnTraversalFinished = OnTraversalFinished;
		m_curDistance = 0.0f;
		m_curSpeed = Speed;
	}
}

bool UGenericTwinPath::BuildSpline()
{
	if(m_Waypoints.Num() <= 0)
		return false;

	float curT = 0.0f;
	for(const SWaypoint &waypoint : m_Waypoints)
	{
		m_Spline.Position.AddPoint(curT, waypoint.Transform.GetTranslation());
		m_Spline.Rotation.AddPoint(curT, waypoint.Transform.GetRotation());
		m_Spline.Scale.AddPoint(curT, FVector(1.0, 1.0, 1.0));

		m_Spline.Position.Points.Last().InterpMode = CIM_CurveAuto;
		m_Spline.Rotation.Points.Last().InterpMode = CIM_CurveAuto;
		m_Spline.Scale.Points.Last().InterpMode = CIM_CurveAuto;

		curT += 1.0f;
	}
	m_Spline.UpdateSpline();


	for(const auto &item : m_FloatChannels)
	{
		BuildFloatChannel(item.Key, item.Value);
	}


	return true;
}

void UGenericTwinPath::BuildFloatChannel(const FName &ChannelId, float InvalidValue)
{
	if(m_Waypoints.Last().FloatChannels[ChannelId] == InvalidValue)
	{
		float lastVal = InvalidValue;
		for(const SWaypoint &waypoint : m_Waypoints)
		{
			if(waypoint.FloatChannels[ChannelId] != InvalidValue)
				lastVal = waypoint.FloatChannels[ChannelId];
		}

		if(lastVal != InvalidValue)
		{
			for(int32 i = m_Waypoints.Num() - 1; i >= 0; --i)
			{
				if(m_Waypoints[i].FloatChannels[ChannelId] != InvalidValue)
					break;

				m_Waypoints[i].FloatChannels[ChannelId] = lastVal;
			}
		}
	}

	TArray<float> lengths = { 0.0f };
	for (int32 i = 1; i < m_Waypoints.Num() - 1; ++i)
	{
		lengths.Add( lengths.Last() + m_Spline.GetSegmentLength(i, 1.0f));
	}
	lengths.Add(m_Spline.GetSplineLength());

	int32 startIdx  = 0;
	while(startIdx < m_Waypoints.Num() - 1)
	{
		int32 endIdx = startIdx + 1;
		if(m_Waypoints[endIdx].FloatChannels[ChannelId] != InvalidValue)
		{
			++startIdx;
		}
		else
		{
			for( ;endIdx < m_Waypoints.Num() &&  m_Waypoints[endIdx].FloatChannels[ChannelId] == InvalidValue; ++endIdx);

			const float startLength = lengths[startIdx];
			const float endLength = lengths[endIdx];
			const float deltaLength = endLength - startLength;

			float startVal = m_Waypoints[startIdx].FloatChannels[ChannelId];
			const float endVal = m_Waypoints[endIdx].FloatChannels[ChannelId];
			const float deltaVal = endVal - startVal;

			for(int32 i = startIdx + 1; i < endIdx; ++i)
			{
				const float curLength = lengths[i];
				const float fac = (curLength - startLength) / deltaLength;
				const float curVal = startVal + deltaVal * fac;
				m_Waypoints[i].FloatChannels[ChannelId] = curVal;
			}

			startIdx = endIdx;
		}
	}
}

void UGenericTwinPath::Tick(float DeltaSeconds)
{
	if(m_isActive)
	{
		m_curDistance += m_curSpeed * DeltaSeconds;
		const float curKey = m_Spline.ReparamTable.Eval(m_curDistance, 0.0f);

		FGenericTwinPathKeyData keyData;

		keyData.Transform = FTransform(m_Spline.Rotation.Eval(curKey), m_Spline.Position.Eval(curKey));
		for(const auto &item : m_FloatChannels)
			keyData.FloatChannels.Add(item.Key, InterpolateFloatChannel(item.Key, curKey));

		m_OnAdvanceOnPath.ExecuteIfBound(keyData);

		if(m_curDistance >= m_Spline.ReparamTable.Points.Last().InVal)
		{
			m_OnTraversalFinished.ExecuteIfBound();
			m_isActive = false;
		}

	}
}

float UGenericTwinPath::InterpolateFloatChannel(const FName &ChannelId, float SplineKey)
{
	const int32 idx = static_cast<int32> (SplineKey);
	const int32 nextIdx = FMath::Min(idx + 1, m_Waypoints.Num() - 1);
	const float fac = FMath::Frac(SplineKey);

	return FMath::Lerp(m_Waypoints[idx].FloatChannels[ChannelId], m_Waypoints[nextIdx].FloatChannels[ChannelId], fac);
}

TStatId UGenericTwinPath::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UGenericTwinPath, STATGROUP_Tickables);
}
