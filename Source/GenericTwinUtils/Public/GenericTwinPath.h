/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Components/SplineComponent.h"
#include "GenericTwinPath.generated.h"


USTRUCT(BlueprintType)
struct FGenericTwinPathKeyData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = Default)
	FTransform	Transform;

	UPROPERTY(BlueprintReadOnly, Category = Default)
	TMap<FName, float>	FloatChannels;

};


DECLARE_DYNAMIC_DELEGATE_OneParam(FOnAdvanceOnPath, FGenericTwinPathKeyData, KeyData);
DECLARE_DYNAMIC_DELEGATE(FOnTraversalFinished);

/**
 * 
 */
UCLASS(BlueprintType)
class GENERICTWINUTILS_API UGenericTwinPath	:	public UObject
											,	public FTickableGameObject
{
	GENERATED_BODY()

private:

	struct SWaypoint
	{
		FTransform				Transform;
		TMap<FName, float>		FloatChannels;
	};

public:

	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void AddFloatChannel(const FName &ChannelId, float InvalidValue);

	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void BeginWaypoint();

	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void SetFloatValue(const FName &ChannelId, float Value);

	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void StoreWaypoint(const FTransform &Transform);


	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void AddWaypoint(const FTransform &Transform);

	UFUNCTION(BlueprintCallable, Category="GenericTwin|Utils|Path")
	void StartTraversal(float Speed, FOnAdvanceOnPath OnAdvanceOnPath, FOnTraversalFinished OnTraversalFinished);

	/*
	*	FTickableGameObject
	*/
	virtual void Tick(float DeltaSeconds) override;

	virtual TStatId GetStatId() const override;


private:

	bool BuildSpline();

	void BuildFloatChannel(const FName &ChannelId, float InvalidValue);
	float InterpolateFloatChannel(const FName &ChannelId, float SplineKey);

	FSplineCurves					m_Spline;

	FOnAdvanceOnPath				m_OnAdvanceOnPath;
	FOnTraversalFinished			m_OnTraversalFinished;

	bool							m_isActive = false;

	TArray<SWaypoint>				m_Waypoints;
	SWaypoint						m_curWaypoint;

	TMap<FName, float>				m_FloatChannels;

	float							m_curDistance = 0.0f;
	float							m_curSpeed = 0.0f;

};
